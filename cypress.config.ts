import 'dotenv/config'
import { defineConfig } from 'cypress'
import { beforeRunHook, afterRunHook } from 'cypress-mochawesome-reporter/lib'

export default defineConfig({
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    charts: true,
    reportPageTitle: 'E2E Report',
    embeddedScreenshots: true,
    inlineAssets: true,
    saveAllAttempts: false
  },
  e2e: {
    defaultCommandTimeout: 15000,
    video: true,
    setupNodeEvents (on, config) {
      on('before:run', async (details) => {
        await beforeRunHook(details)
      })

      on('after:run', async () => {
        await afterRunHook()
      })

      config.baseUrl = process.env.BASE_URL ?? null
      config.userAgent = process.env.USER_AGENT ?? null

      config.env = {
        ...process.env,
        ...config.env
      }

      return config
    }
  }
})
