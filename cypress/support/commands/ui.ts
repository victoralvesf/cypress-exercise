import { account as accountLocators } from '@/support/locators/index'
import { paths } from '@/support/utils/paths'

const { signInForm, registerForm } = accountLocators

interface TypeOptions extends Cypress.TypeOptions {
  sensitive: boolean
}

Cypress.Commands.overwrite<'type', 'element'>(
  'type',
  (originalFn, element, text, options?: Partial<TypeOptions>) => {
    if (options !== null && options?.sensitive !== undefined) {
      options.log = false

      Cypress.log({
        $el: element,
        name: 'type',
        message: '*'.repeat(text.length)
      })
    }

    return originalFn(element, text, options)
  }
)

const sensitive: Partial<TypeOptions> = {
  sensitive: true
}

Cypress.Commands.add('fillAccountForm', (account) => {
  cy.get(registerForm.firstName).type(account.firstName)
  cy.get(registerForm.lastName).type(account.lastName)

  cy.get(registerForm.email).type(account.email)

  cy.get(registerForm.password).type(account.password, sensitive)
  cy.get(registerForm.passwordConfirmation).type(account.password, sensitive)

  cy.contains('button', 'Create an Account').click()
})

Cypress.Commands.add('login', (account) => {
  cy.visit(paths.signIn)

  cy.get(signInForm.email).type(account.email)
  cy.get(signInForm.password).type(account.password, sensitive)

  cy.contains('button', 'Sign In').click()
})

Cypress.Commands.add('clickButton', (text) => {
  cy.contains('button', text).click()
})

Cypress.Commands.add('clickLink', (text) => {
  cy.contains('a', text).click()
})
