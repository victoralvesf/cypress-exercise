import { type RequestOptions } from '@/support'

Cypress.Commands.add('createUser', (account, address) => {
  const requestOptions: RequestOptions = {
    url: '/rest/all/V1/customers/isEmailAvailable',
    method: 'POST',
    body: {
      customerEmail: account.email,
      websiteId: 1
    }
  }

  cy.request(requestOptions).then((response) => {
    expect(response.status).to.equal(200)

    if (response.body === 'false') return

    const createUserRequest: RequestOptions = {
      url: '/rest/all/V1/customers',
      method: 'POST',
      body: {
        customer: {
          email: account.email,
          firstname: account.firstName,
          lastname: account.lastName,
          disable_auto_group_change: 0,
          addresses: address !== undefined ? [address] : []
        },
        password: account.password
      }
    }

    cy.request(createUserRequest).then((response) => {
      expect(response.status).to.equal(200)
    })
  })
})
