import { type IAccount } from '../types/interfaces/account'

export {}

declare global {
  namespace Cypress {
    interface Chainable {
      /**
       * Custom command to fill register form.
       * @example cy.fillAccountForm(account)
       */
      fillAccountForm: (account: IAccount) => Chainable<void>
      /**
       * Custom command login.
       * @example cy.login(account)
       */
      login: (account: IAccount) => Chainable<void>
      /**
       * Custom command to create a user using REST API.
       * @example cy.createUser(account)
       */
      createUser: (account: IAccount, address?: object) => Chainable<void>
      /**
       * Custom command to click a button by text.
       * @example cy.clickButton('Add to Cart')
       */
      clickButton: (text: string) => Chainable<void>
      /**
       * Custom command to click a text link.
       * @example cy.clickLink('learn more')
       */
      clickLink: (text: string) => Chainable<void>
    }
  }
}

export interface RequestOptions extends Partial<Cypress.RequestOptions> {}
