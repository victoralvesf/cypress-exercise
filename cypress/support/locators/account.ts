export const account = {
  title: '.base',
  informations: '#maincontent .box-information',
  registerSuccessMessage: '.message-success',
  registerFailureMessage: '.message-error',
  registerForm: {
    firstName: '#firstname',
    lastName: '#lastname',
    email: '#email_address',
    password: '#password',
    passwordConfirmation: '#password-confirmation'
  },
  signInForm: {
    email: '#email',
    password: '#pass'
  }
}
