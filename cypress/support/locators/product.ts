export const product = {
  title: '[data-ui-id=page-title-wrapper]',
  sizeButton: 'div[id*=label-size]',
  colorButtonByName: (color: string) => `div[id*=label-color][data-option-label=${color}]`,
  addToCartSuccessMessage: '[data-ui-id=message-success]'
}
