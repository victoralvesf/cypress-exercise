export const checkout = {
  shipping: {
    summary: '.opc-block-summary',
    info: 'li#shipping',
    methods: {
      info: '#opc-shipping_method',
      tableRate: 'input[name=ko_unique_1][value*=tablerate]',
      flatRate: 'input[name=ko_unique_2][value*=flatrate]'
    }
  },
  reviewAndPayment: {
    order: {
      subtotal: 'tr.grand.totals'
    },
    billing: {
      addressSameAsShipping: 'input[name=billing-address-same-as-shipping]'
    },
    shipping: {
      summary: '.opc-block-shipping-information'
    }
  },
  success: {
    title: 'h1.page-title',
    orderInfo: 'div.checkout-success'
  }
}
