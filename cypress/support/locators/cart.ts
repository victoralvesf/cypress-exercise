export const cart = {
  showPreview: '.showcart',
  previewSubtotal: '#minicart-content-wrapper .subtotal',
  countProducts: '[data-block=minicart] .counter-number'
}
