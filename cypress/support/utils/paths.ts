export const paths = {
  register: '/customer/account/create',
  signIn: '/customer/account/login',
  account: '/customer/account',
  products: {
    jackets: '/men/tops-men/jackets-men.html'
  }
}
