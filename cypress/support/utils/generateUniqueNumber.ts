import { faker } from '@faker-js/faker'

export function generateUniqueNumber (): string {
  const currentDate = new Date().toLocaleDateString('pt-BR')
  const dateHash = currentDate.replace(/\//g, '')
  const randomNumbers = faker.number.int({ min: 100, max: 999 })

  return dateHash + randomNumbers.toString()
}
