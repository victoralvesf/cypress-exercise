export interface IAccount {
  firstName: string
  lastName: string
  fullName: string
  email: string
  password: string
}
