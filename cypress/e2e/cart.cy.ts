import { cart, product } from '@/support/locators'
import { paths } from '@/support/utils/paths'

describe('cart', () => {
  it('should add a mens jacket to the cart', () => {
    cy.visit(paths.products.jackets)

    const productName = 'Beaumont Summit Kit'

    cy.clickLink(productName)
    cy.get(product.title).should('have.text', productName)

    cy.get(product.sizeButton).contains('XL').as('productSize')
    cy.get('@productSize').click()
    cy.get('@productSize')
      .should('have.attr', 'aria-checked', 'true')
      .and('have.class', 'selected')

    cy.get(product.colorButtonByName('Orange')).as('orangeProduct')
    cy.get('@orangeProduct').click()
    cy.get('@orangeProduct')
      .should('have.attr', 'aria-checked', 'true')
      .and('have.class', 'selected')

    cy.clickButton('Add to Cart')

    cy.get(product.addToCartSuccessMessage).should('contain.text', `You added ${productName} to your shopping cart.`)
    cy.get(cart.countProducts).should('have.text', '1')
  })
})
