import { createAccountFixture } from '@/fixtures/createAccountFixture'
import { account } from '@/support/locators'
import { paths } from '@/support/utils/paths'

describe('account', () => {
  describe('register', () => {
    beforeEach(() => {
      cy.visit(paths.register)
    })

    it('should create a new account', () => {
      const newAccount = createAccountFixture()
      cy.fillAccountForm(newAccount)

      cy.get(account.registerSuccessMessage)
        .should('contain.text', 'Thank you for registering with Main Website Store.')
    })

    it('should not create an account if it already exists', () => {
      const existingAccount = createAccountFixture()
      cy.createUser(existingAccount)

      cy.fillAccountForm(existingAccount)

      cy.get(account.registerFailureMessage)
        .should('contain.text', 'There is already an account with this email address. If you are sure that it is your email address, click here to get your password and access your account.')
    })
  })

  describe('login', () => {
    it('should sign in successfully', () => {
      const accountToSignIn = createAccountFixture()
      cy.createUser(accountToSignIn)

      cy.login(accountToSignIn)

      cy.get(account.title).should('have.text', 'My Account')

      cy.get(account.informations)
        .should('contain.text', accountToSignIn.fullName)
        .and('contain.text', accountToSignIn.email)
    })
  })
})
