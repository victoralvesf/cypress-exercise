import { accountAddressFixture } from '@/fixtures/accountAddressFixture'
import { accountFixture } from '@/fixtures/accountFixture'
import { cart, checkout, layout, product } from '@/support/locators'

describe('purchase', () => {
  it('should search for a product and buy it', () => {
    cy.createUser(accountFixture, accountAddressFixture)
    cy.login(accountFixture)

    cy.visit('/')

    const productName = 'Fusion Backpack'

    cy.get(layout.searchInput).type(`${productName}{enter}`)

    cy.clickLink(productName)
    cy.get(product.title).should('have.text', productName)

    cy.clickButton('Add to Cart')

    cy.get(product.addToCartSuccessMessage).should('contain.text', `You added ${productName} to your shopping cart.`)
    cy.get(cart.countProducts).should('have.text', '1')

    cy.get(cart.showPreview).click()
    cy.get(cart.previewSubtotal)
      .should('contain.text', 'Cart Subtotal')
      .and('contain.text', '$59.00')

    cy.clickButton('Proceed to Checkout')

    cy.get(checkout.shipping.summary).should('contain.text', 'Order Summary')

    cy.get(checkout.shipping.info)
      .should('contain.text', 'Shipping Address')
      .should('contain.text', accountFixture.firstName)
      .should('contain.text', accountFixture.lastName)
      .should('contain.text', accountAddressFixture.street[0])
      .should('contain.text', accountAddressFixture.region.region)
      .should('contain.text', accountAddressFixture.city)

    cy.get(checkout.shipping.methods.info).should('contain.text', 'Shipping Methods')

    cy.get(checkout.shipping.methods.flatRate).check()

    cy.clickButton('Next')

    cy.get(checkout.reviewAndPayment.billing.addressSameAsShipping).should('be.checked')
    cy.get(checkout.reviewAndPayment.order.subtotal).should('contain.text', '$64.00')

    cy.get(checkout.reviewAndPayment.shipping.summary)
      .should('contain.text', 'Ship To:')
      .should('contain.text', accountFixture.firstName)
      .should('contain.text', accountFixture.lastName)
      .should('contain.text', accountAddressFixture.street[0])
      .should('contain.text', accountAddressFixture.region.region)
      .should('contain.text', accountAddressFixture.city)
      .should('contain.text', 'Shipping Method:')
      .should('contain.text', 'Flat Rate - Fixed')

    cy.clickButton('Place Order')

    cy.get(checkout.success.title).should('contain.text', 'Thank you for your purchase!')

    cy.get(checkout.success.orderInfo)
      .should('contain.text', 'Your order number is:')
      .should('contain.text', "We'll email you an order confirmation with details and tracking info.")
  })
})
