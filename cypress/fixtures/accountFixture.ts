import { generateUniqueNumber } from '@/support/utils/generateUniqueNumber'
import { type IAccount } from '@/types/interfaces/account'

export const accountFixture: IAccount = {
  firstName: 'John',
  lastName: 'McClane',
  fullName: 'John McClane',
  email: `john_mcclane_${generateUniqueNumber()}@nypd.test`,
  password: Cypress.env('ACCOUNT_PASSWORD') as string
}
