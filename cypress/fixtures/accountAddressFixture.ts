import { accountFixture } from './accountFixture'

export const accountAddressFixture = {
  region: {
    region_code: 'NY',
    region: 'New York',
    region_id: 43
  },
  region_id: 43,
  country_id: 'US',
  street: [
    '211 Union Avenue'
  ],
  company: 'NYPD',
  telephone: '(718) 936-3511',
  postcode: '11202',
  city: 'Brooklyn',
  firstname: accountFixture.firstName,
  lastname: accountFixture.lastName,
  default_shipping: true,
  default_billing: true
}
