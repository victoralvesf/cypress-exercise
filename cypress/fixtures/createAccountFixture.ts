import { faker } from '@faker-js/faker'
import { type IAccount } from '@/types/interfaces/account'

export function createAccountFixture (): IAccount {
  const firstName = faker.person.firstName()
  const lastName = faker.person.lastName()

  return {
    firstName,
    lastName,
    fullName: [firstName, lastName].join(' '),
    email: faker.internet.email({ firstName, lastName }),
    password: Cypress.env('ACCOUNT_PASSWORD') as string
  }
}
