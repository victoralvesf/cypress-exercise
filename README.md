# Cypress Exercise

[![pipeline status](https://gitlab.com/victoralvesf/cypress-exercise/badges/main/pipeline.svg)](https://gitlab.com/victoralvesf/cypress-exercise/-/commits/main)

## Pré-requisitos

Antes de começar, certifique-se de ter as seguintes ferramentas instaladas em sua máquina:

- [Node.js](https://nodejs.org/) (versão 18 ou superior)
- [npm](https://www.npmjs.com/) (normalmente é instalado junto com o Node.js)

## Instalação

1. Clone o repositório

```bash
git clone https://gitlab.com/victoralvesf/cypress-exercise.git

cd cypress-exercise
```

2. Instale as Dependências

```bash
npm install
```

3. Configure as variáveis de ambiente

- Copiando o arquivo de exemplo

```bash
# linux
cp .env.example .env
```

```bash
# windows
copy .env.example .env
```

Após realizar a cópia do arquivo de exemplo, abra o arquivo `.env` e preencha as variáveis com os valores correspondentes.

- `ACCOUNT_PASSWORD` deve ser preenchida com um valor aleatório, recomendo o uso de um gerador de senhas.

## Execução dos Testes

1. Usando a UI do Cypress

```bash
npm run test:ui
```

Esse comando irá abrir a interface do Cypress, para que os testes sejam executados de forma interativa.

2. Executando os testes via linha de comando

```bash
# Usando Electron
npm run test

# Usando Chrome
npm run test:chrome
```

Esses comandos irão executar os testes no modo headless.

## Casos de teste

- Todos os casos de teste estão independentes e podem ser executados de forma isolada.

| Descrição | Status |
| --------- | ------ |
| Criar uma conta | <center>✅</center> |
| Não deve criar uma conta com um e-mail já utilizado | <center>✅</center> |
| Login | <center>✅</center> |
| Adicionar um produto ao carrinho pela página de produtos | <center>✅</center> |
| Adicionar um produto ao carrinho usando a busca e finalizando a compra | <center>✅</center> |